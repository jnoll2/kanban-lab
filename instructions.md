---
title: #kanban_host URL deliverable
author: #module_leder
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Objectives

For this exercise, we will create a Kanban board using the service at
Trello.com.  You may want to review the Project planning tutorial
notes regarding Kanban first.

# Tasks

## Part 1: Create a Kanban board using Trello.


#. Register for a Trello account.  Visit #url(www.trello.com), and
 enter your email address in the text box next to the green "Sign Up - It's Free!" button:\
    ![Trello Sign-up](trello-signup.png)\
    

#. Select a member of your group to be the Trello board owner.  Your group should have _one_ board to manage the project plan.


#. Create a new Kanban board.    Trello will ask you to specify a name
 for your Kanban board.  Name it after your group: if your group is "a_group 999" your board should be called "a_group999" (no spaces):\
    ![Kanban board name](trello-board-title.png)\
    

#. Create four lists (columns) called "Backlog," "Doing," "Review," and "Done."\
    ![Kanban columns](trello-four-lists.png)\
    

#. Add some cards to the "Backlog" column.\
    ![Kanban columns](trello-add-card.png)\


#. Add a "member" (not me!) to the card.  This person is the responsible party.\
    ![Add member](trello-add-member.png)\


#. Click the "Share" button to add members.\
    ![Invite ML](trello-invite-ml-2.png)\


#. Invite the module team to join the board: #email(#module_email) (#module_bitbucket_id)\
    ![Invite ML](trello-invite-ml-2.png)\
 

#. Copy your Trello board URL.\
    ![Find URL](trello-find-url.png)\
    

#. Paste your Trello board URL into the assignment submission box.\
    ![Submit URL](canvas-submit-url.png)\
    

#. Verify your URL!

    Someone other than the submitter should copy the URL from Canvas
    to a web browser to be sure the submitter submitted the correct URL.

    You should submit _one_ and only _one_ Trello board URL: it should be the URL of the board you are using to manage your project plan.
